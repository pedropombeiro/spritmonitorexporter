[![pipeline status](https://gitlab.com/pedropombeiro/spritmonitorexporter/badges/master/pipeline.svg)](https://gitlab.com/pedropombeiro/spritmonitorexporter/-/commits/master)

# spritmonitorexporter

`spritmonitorexporter` is a simple Go program meant to be run in the background in order to export
relevant spritmonitor.de metrics to Influxdb. It exposes an HTTP `/metrics` endpoint which generates a standard Influxdb line protocol file.

## Installation

1. Download the latest spritmonitorexporter executable from the [Releases page](https://gitlab.com/pedropombeiro/spritmonitorexporter/-/releases)
1. Run `spritmonitorexporter`

    ```shell
    ./spritmonitorexporter
    ```

...

## Customization

spritmonitorexporter supports the following command line flags:

| Flag                    | Default value | Description |
|-------------------------|---------------|-------------|
| `--port`                | `:9094`       | Address/port where to serve the metrics  |
| `--username`            |               | Account user name                        |
| `--password`            |               | Account password                         |
| `--car-id`              |               | ID of the car to scrape                  |
| `--healthcheck`         | N/A           | Healthcheck service to ping every 5 minutes (currently supported: `healthchecks.io:<check-id>`)  |
| `--log`                 | N/A           | Path to log file (defaults to standard output)  |

## Tips

The root endpoint exposes information about the current status of the program (useful for debugging).
