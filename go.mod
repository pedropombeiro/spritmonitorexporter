module gitlab.com/pedropombeiro/spritmonitorexporter

// +heroku goVersion go1.14
go 1.14

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/go-ping/ping v0.0.0-20201115131931-3300c582a663
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mackerelio/go-osstat v0.1.0
	github.com/robbiet480/go.nut v0.0.0-20200921180721-77b33bf222d9
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.7.0
	gitlab.com/pedropombeiro/qnapexporter v0.0.6 // indirect
	gitlab.com/pedropombeiro/spritmonitorscraper v0.0.0-20210128220344-a4dc017a91c1
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	golang.org/x/sys v0.0.0-20210112080510-489259a85091 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
