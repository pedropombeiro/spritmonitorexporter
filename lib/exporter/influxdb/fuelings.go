package influxdb

import (
	"fmt"
	"io"
	"io/ioutil"
	"strconv"
	"strings"
	"time"

	scraper "gitlab.com/pedropombeiro/spritmonitorscraper"
)

func (e *promExporter) getFuelingMetrics() ([]metric, error) {
	retries := 2

	var b io.ReadCloser
	var err error
	for retries > 0 {
		retries--
		if e.sessid == "" {
			sessid, err := scraper.Login(e.Username, e.Password)
			if err != nil {
				return nil, fmt.Errorf("logging in: %w", err)
			}
			e.sessid = sessid
		}

		b, err = scraper.GetFuelingsCSV(e.sessid, e.CarID)
		if err == nil {
			break
		}

		e.sessid = ""
		if retries == 0 {
			return nil, fmt.Errorf("fetching fuelings: %w", err)
		}
	}
	defer b.Close()

	buf, err := ioutil.ReadAll(b)
	if err != nil {
		return nil, fmt.Errorf("reading response: %w", err)
	}

	lines := strings.Split(string(buf), "\n")
	metrics := make([]metric, 0, len(lines)*2)
	for _, line := range lines[1:] {
		fields := strings.SplitN(line, ";", 5)

		var (
			timestamp time.Time
			odometer  float64
			quantity  float64
		)
		timestamp, err = time.ParseInLocation("02.01.2006", fields[0], time.Local)
		if err != nil {
			return nil, err
		}
		odometer, err = strconv.ParseFloat(strings.Replace(fields[1], ",", ".", 1), 64)
		if err != nil {
			return nil, err
		}
		quantity, err = strconv.ParseFloat(strings.Replace(fields[3], ",", ".", 1), 64)
		if err != nil {
			return nil, err
		}

		timestamp = timestamp.Add(12 * time.Hour)
		metrics = append(
			metrics,
			metric{
				name:      "fueling_odometer_kms",
				value:     odometer,
				timestamp: timestamp,
			},
			metric{
				name:      "fueling_volume_lts",
				value:     quantity,
				timestamp: timestamp,
			},
		)
	}

	return metrics, nil
}
