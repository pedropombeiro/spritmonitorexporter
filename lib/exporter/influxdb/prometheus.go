package influxdb

import (
	"fmt"
	"io"
	"log"
	"sync"
	"time"

	"gitlab.com/pedropombeiro/spritmonitorexporter/lib/exporter"
)

type fetchMetricFn func() ([]metric, error)

type promExporter struct {
	ExporterConfig

	sessid string

	status *exporter.Status

	fns     []fetchMetricFn
	fetchMu sync.Mutex
}

type ExporterConfig struct {
	Username, Password string
	CarID              int
	Logger             *log.Logger
}

func NewExporter(config ExporterConfig, status *exporter.Status) exporter.Exporter {
	now := time.Now()
	e := &promExporter{
		ExporterConfig: config,
		status:         status,
	}
	e.fns = []fetchMetricFn{
		e.getFuelingMetrics, // #1
	}

	if status != nil {
		status.Uptime = now
	}

	return e
}

func (e *promExporter) WriteMetrics(w io.Writer) error {
	e.fetchMu.Lock()
	defer e.fetchMu.Unlock()

	if e.status != nil {
		e.status.MetricCount = 0
		e.status.LastFetch = time.Now()
		defer func() {
			e.status.LastFetchDuration = time.Since(e.status.LastFetch)
		}()
	}

	var wg sync.WaitGroup
	metricsCh := make(chan interface{}, 4)
	for idx, fn := range e.fns {
		wg.Add(1)

		go fetchMetricsWorker(&wg, metricsCh, idx, fn)
	}

	go func() {
		// Close channel once all workers are done
		wg.Wait()
		close(metricsCh)
	}()

	// Retrieve metrics from channel and write them to the response
	for m := range metricsCh {
		switch v := m.(type) {
		case []metric:
			if e.status != nil {
				e.status.MetricCount += len(v)
			}
			for _, m := range v {
				writeMetricMetadata(w, m)

				_, _ = fmt.Fprintf(w, "consumption,%s=%g", e.getMetricFullName(m), m.value)
				if !m.timestamp.IsZero() {
					_, _ = fmt.Fprintf(w, " %d", m.timestamp.UnixNano())
				}
				_, _ = fmt.Fprintln(w)
			}
		case error:
			e.Logger.Println(v.Error())

			_, _ = fmt.Fprintf(w, "## %v\n", v)
		}
	}

	return nil
}

func fetchMetricsWorker(wg *sync.WaitGroup, metricsCh chan<- interface{}, idx int, fetchMetricsFn fetchMetricFn) {
	defer wg.Done()

	metrics, err := fetchMetricsFn()
	if err != nil {
		metricsCh <- fmt.Errorf("retrieve metric #%d: %w", 1+idx, err)
		return
	}

	metricsCh <- metrics
}

func (e *promExporter) Close() {
}

func (e *promExporter) getMetricFullName(m metric) string {
	if m.attr != "" {
		return fmt.Sprintf(`car=%d,%s %s`, e.CarID, m.attr, m.name)
	}

	return fmt.Sprintf(`car=%d %s`, e.CarID, m.name)
}

func writeMetricMetadata(w io.Writer, m metric) {
}
