package influxdb

import (
	"io/ioutil"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewExporter(t *testing.T) {
	config := ExporterConfig{
		CarID:  1,
		Logger: log.New(ioutil.Discard, "", 0),
	}
	e := NewExporter(config, nil)

	require.NotNil(t, e)
	assert.IsType(t, &promExporter{}, e)
}
